package net.curlybraces.date_time_api;

import java.time.LocalDateTime;
import java.time.Month;

public class LocalDateTimeMain {

	public static void main(String[] args) {

		// LocalDateTime: represent a combination of date and time.
		LocalDateTime now = LocalDateTime.now();
		System.out.println(now);
		
		// of function
		LocalDateTime localDateTime = LocalDateTime.of(2019, Month.MAY, 11, 19, 45);
		System.out.println(localDateTime);
		
		// parse function
		localDateTime = LocalDateTime.parse("2019-05-20T06:30:00");
		System.out.println(localDateTime);
		System.out.println(localDateTime.getHour());
		
		// Addition & Subtraction methods are also available
		
		// Getter methods are available to extract specific units similar to the date and time classes.
	}

}
