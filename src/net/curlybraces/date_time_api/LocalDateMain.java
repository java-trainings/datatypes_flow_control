package net.curlybraces.date_time_api;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class LocalDateMain {

	public static void main(String[] args) {

		// LocalDate : represents a date in ISO format (yyyy-MM-dd) without time
		LocalDate localDate = LocalDate.now();
		System.out.println(localDate);
		
		// Using of function
		localDate = LocalDate.of(2019, Month.MAY, 12);
		System.out.println(localDate);
		// or parse method
		localDate = LocalDate.parse("2019-11-12");
		System.out.println(localDate);
		
		// Some utility functions
		// Addition & subtraction of dates
		//LocalDate tomorrow = LocalDate.now().plusDays(1);
		LocalDate tomorrow = LocalDate.now().plus(1, ChronoUnit.DAYS);
		System.out.println(tomorrow);
		
		LocalDate afterTwoMonth = LocalDate.now().plus(2, ChronoUnit.MONTHS);
		System.out.println(afterTwoMonth);
		
		LocalDate sameDayLastYear = LocalDate.now().minus(1, ChronoUnit.YEARS);
		System.out.println(sameDayLastYear);
		
		// get day of week or of month
		DayOfWeek birthday = LocalDate.parse("2009-05-05").getDayOfWeek();
		System.out.println("Birthday of my young daughter = " + birthday);
		
		int today = LocalDate.now().getDayOfMonth();
		System.out.println("today in the month = " + today);
		
		//Leap year
		System.out.println(localDate.isLeapYear());
		System.out.println(LocalDate.parse("2000-01-01").isLeapYear());
		
		// After and Before a date checks
		System.out.println(LocalDate.parse("2019-05-12").isBefore(LocalDate.parse("2019-05-19"))); 
		System.out.println(LocalDate.parse("2019-05-12").isAfter(LocalDate.parse("2019-09-12")));
	}

}
