package net.curlybraces.date_time_api;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class LocalTimeMain {

	public static void main(String[] args) {

		// LocalTime: represents time without a date.
		// current LocalTime
		LocalTime now = LocalTime.now();
		System.out.println(now);
		
		// Using parse function
		LocalTime midnightTirthy = LocalTime.parse("00:30:59");
		System.out.println(midnightTirthy);
		
		// Using of function
		LocalTime sixThirty = LocalTime.of(18, 30);
		System.out.println(sixThirty);
		
		// Addition & Subtraction
		LocalTime seven = sixThirty.plus(30, ChronoUnit.MINUTES);
		System.out.println(seven);
		LocalTime fiveThirty = sixThirty.minus(1, ChronoUnit.HOURS);
		System.out.println(fiveThirty);
		
		// After and Before checks are also available, the same way as LocalDate
		
		// The max, min and noon time of a day can be obtained by constants in LocalTime class. 
		// This is very useful when performing database queries to find records within a given span of time
		LocalTime maxTime = LocalTime.MAX;
		System.out.println(maxTime);
	}

}
