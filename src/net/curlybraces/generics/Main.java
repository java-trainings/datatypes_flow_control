package net.curlybraces.generics;

import java.math.BigDecimal;

public class Main {

	@SuppressWarnings({"unchecked","rawtypes"})
	public static void main(String[] args) {
		
		// Generic & interfaces
		ClassImplementingGenericInterface classImplementingGenericInterface = new ClassImplementingGenericInterface();
		classImplementingGenericInterface.performAction("SLAP");
		
		Class2ImplementingGenericInterface class2ImplementingGenericInterface = new Class2ImplementingGenericInterface();
		class2ImplementingGenericInterface.performAction(new BigDecimal(-200));
		
		GenericInterfaceOneType<Integer> genericInterfaceOneType = new GenericInterfaceOneType<Integer>() {
			
			@Override
			public void performAction(Integer action) {
				System.out.println(action * 10);
				
			}
		};
		
		genericInterfaceOneType.performAction(20);
		
		// Generic & classes
		GenericsTypeOld oldType = new GenericsTypeOld();
		oldType.set("Java Generics"); 
		String str = (String) oldType.get(); //type casting, error prone and can cause ClassCastException
		System.out.println(str);
		
		GenericsType<String> genericType = new GenericsType<>();
		genericType.set("Java Generics"); //valid
		
		GenericsType type1 = new GenericsType(); //raw type
		type1.set("Java Generics"); //valid
		type1.set(10); //valid and auto-boxing support
		
		// Generic methods
		GenericsType<String> g1 = new GenericsType<>();
		g1.set("Java Generics ");
		
		GenericsType<String> g2 = new GenericsType<>();
		g2.set("Java Generics");
		
		boolean isEqual = GenericsMethods.<String>isEqual(g1, g2);
		//above statement can be written simply as
		isEqual = GenericsMethods.isEqual(g1, g2);
		System.out.println("isEquel = " + isEqual);
		//This feature, known as type inference, allows you to invoke a generic method as an ordinary method, without specifying a type between angle brackets.
		//Compiler will infer the type that is needed
	}

}
