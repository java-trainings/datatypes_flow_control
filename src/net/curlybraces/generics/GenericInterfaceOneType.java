package net.curlybraces.generics;

public interface GenericInterfaceOneType< T > {
	void performAction( T action );
}

