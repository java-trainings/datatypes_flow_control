package net.curlybraces.generics;

public class ClassImplementingGenericInterface implements GenericInterfaceOneType< String > {

	@Override
	public void performAction(String action) {
		System.out.println("Perform action = " + action);
	}
}
