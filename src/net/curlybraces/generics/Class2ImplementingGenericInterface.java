package net.curlybraces.generics;

import java.math.BigDecimal;

public class Class2ImplementingGenericInterface implements GenericInterfaceOneType<BigDecimal> {

	@Override
	public void performAction(BigDecimal action) {
		System.out.println(action.signum());
	}
}
