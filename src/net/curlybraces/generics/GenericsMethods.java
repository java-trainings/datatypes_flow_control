package net.curlybraces.generics;

public class GenericsMethods {

	// Java Generic Method
	public static <T> boolean isEqual(GenericsType<T> g1, GenericsType<T> g2) {
		return g1.get().equals(g2.get());
	}
}
