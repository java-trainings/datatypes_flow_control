package net.curlybraces.variables_datatypes_operators;

public class VarDataTypes {

	public static void main(String[] args) {

		System.out.println("Hello Mohamed To Advanced java Course\n\n");
		// byte 1 byte
		byte byteValue = 127;
		System.out.println("byteValue :: " + byteValue);
		// short 2 bytes
		short shortValue = -32768;
		System.out.println("shortValue :: " + shortValue);
		// int 4 bytes
		int intValue = 2_147_483_647;
		System.out.println("intValue :: " + intValue);
		// long 8 bytes
		long longValue = 9_223_372_036_854_775_807L;
		System.out.println("longValue :: " + longValue);
		// float 4 bytes
		float floatValue = 2_147_483_649f;
		System.out.println("floatValue :: " + floatValue);
		// double 8 bytes
		double doubleValue = 5.5d;
		System.out.println("doubleValue :: " + doubleValue);
		// char :: https://unicode-table.com/en/#control-character
		char charValue = '\u00AE';
		System.out.println("charValue :: " + charValue);
		// boolean true or false
		boolean booleanValue = true;
		System.out.println("booleanValue :: " + booleanValue);
		
		int newValue = 6;
		if (newValue == 6) {
			System.out.println("This is correct");
		}
		
		boolean isAlien = false;
		if (isAlien = true) {
			System.out.println("This should not happen; isAlien = " + isAlien);
		}
		
		// Operators summary
		// https://docs.oracle.com/javase/tutorial/java/nutsandbolts/opsummary.html
		// Operators Precedences
		// http://www.cs.bilkent.edu.tr/~guvenir/courses/CS101/op_precedence.html
		
	}

}
