package net.curlybraces.classes_interfaces_inheritence.interfaces;

public interface SimpleInterface {

	// this is the contract
	void performAction();

}
