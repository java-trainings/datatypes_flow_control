package net.curlybraces.classes_interfaces_inheritence.interfaces;

public abstract class SimpleAbstractClass {

	// Abstract classes are very useful when most or even some part of implementation details could be shared by many
	// subclasses. However, they still leave the door open and allow customizing the intrinsic behavior of each subclass by means of
	// abstract methods
	
	public void performAction() {
		// Implementation here
		System.out.println("Perform an action");
	}

	public abstract void performAnotherAction();
	
	// One thing to mention, in contrast to interfaces which can contain only public declarations, abstract classes may use the full
	// power of accessibility rules to control abstract methods visibility
}
