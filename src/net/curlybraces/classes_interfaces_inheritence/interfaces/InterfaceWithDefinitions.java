package net.curlybraces.classes_interfaces_inheritence.interfaces;

public interface InterfaceWithDefinitions {

	// As for the constant field declarations, additionally to being public, they are implicitly static and final
	// so the declaration of CONSTANT is also equivalent to
	// public static final String CONSTANT = "CONSTANT";
	String CONSTANT = "CONSTANT";
	
	enum InnerEnum {
		E1, E2;
	}
	
	// The nested classes, interfaces or enumerations, additionally to being public, are implicitly declared as static
	// so the InnerClass declaration is also equivalent to 
	// static class InnerClass {}
	class InnerClass {
	}
	
	interface InnerInterface {
		void performInnerAction();
	}
	
	void performAction();
	// is equivalent to 
	// public void performAction();
	// and also equivalent to 
	// public abstract void performAction();

}
