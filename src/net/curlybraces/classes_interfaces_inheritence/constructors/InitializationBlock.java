package net.curlybraces.classes_interfaces_inheritence.constructors;

public class InitializationBlock {

	private String arg1;
	private String arg2;
	private String arg3;
	// Java has yet another way to provide initialization logic using initialization blocks.
	// This feature is rarely used but it is better to know it exists.
	
	// The particular class may have multiple initialization blocks and they all will 
	// be called in the order they are defined in the code
	
	// Initialization blocks do not replace the constructors and may be used along with them. 
	// But it is very important to mention that	initialization blocks are always called before any constructor
	
	{
		this.arg1 = "Initialized in block";
		this.arg3 = "Initialized in first block";
	}
	
	{
		this.arg3 = "Override Initialization in the second block";
	}
	
	public InitializationBlock() {
		this.arg2 = "Initialized in Constructor";
		this.arg1 = "Override Initialization in block";
	}
	
	public String getArg1() {
		return arg1;
	}
	
	public String getArg2() {
		return arg2;
	}
	
	public String getArg3() {
		return arg3;
	}
}
