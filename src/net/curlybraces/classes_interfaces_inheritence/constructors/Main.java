package net.curlybraces.classes_interfaces_inheritence.constructors;

public class Main {

	public static void main(String[] args) {
		
		NoConstructor noConstructor = new NoConstructor();
		System.out.println(noConstructor.toString());
		
		NoArgConstructor noArgConstructor = new NoArgConstructor();
		System.out.println(noArgConstructor.toString());
		
		ConstructorWithArguments constructorWithArguments = new ConstructorWithArguments("TEST-1", true);
		System.out.println(constructorWithArguments);
		
		ConstructorWithArguments constructorWithLessArguments = new ConstructorWithArguments("TEST-2");
		System.out.println(constructorWithLessArguments);
		
		InitializationBlock initializationBlock = new InitializationBlock();
		System.out.println("\nARG1 = " + initializationBlock.getArg1() 
			+ "\nARG2 = " + initializationBlock.getArg2()
			+ "\nARG3 = " + initializationBlock.getArg3());
		
		StaticInitializationBlock staticInitializationBlock = new StaticInitializationBlock();
		System.out.println(staticInitializationBlock);
		
		StaticInitializationBlock staticInitializationBlock2 = new StaticInitializationBlock();
		System.out.println(staticInitializationBlock2);

	}

}

// Code of Object.toString()
//public String toString() {
// return getClass().getName() + "@" + Integer.toHexString(hashCode());
//}