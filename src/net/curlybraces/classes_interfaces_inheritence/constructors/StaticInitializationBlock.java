package net.curlybraces.classes_interfaces_inheritence.constructors;

public class StaticInitializationBlock {

	// class-level initialization constructs called static initializers. 
	// There are very similar to the initialization blocks except for the additional static keyword.
	// Please notice that static initialization is performed once per class-loader
	
	private static String arg1;
	private String arg2;
	
	static {
		arg1 = "First initialization for arg1";
	}
	
	public StaticInitializationBlock() {
		System.out.println("ARG1 in the constructor = " + arg1);
		arg1 = "Overrided in the constructor";
		this.arg2 = "First initialization for arg2";
	}
	
	@Override
	public String toString() {
		return arg1.concat("\n").concat(arg2);
	}
}
