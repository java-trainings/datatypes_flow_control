package net.curlybraces.classes_interfaces_inheritence.constructors;

public class NoArgConstructor {
	// No Arg Constructor : The constructor without arguments (or no-arg constructor) 
	// is the simplest way to do Java compilerís job explicitly
	public NoArgConstructor() {
		// Constructor body here
	}

}
