package net.curlybraces.classes_interfaces_inheritence.constructors;

public class ConstructorWithArguments {
	private String argument1;
	private boolean argument2;
	// The constructors with arguments are the most interesting 
	// and useful way to parameterize new class instances creation
	public ConstructorWithArguments(String arg1, boolean arg2) {
		// Constructor body here
		// Example
		this.argument1 = arg1;
		this.argument2 = arg2;
	}
	
	// the constructors can call each other using the special this keyword. It is considered a good practice to chain
	// constructors in such a way as it reduces code duplication and basically leads to having single initialization entry point
	public ConstructorWithArguments(String arg1) {
		this(arg1, false);
	}
	
	@Override
	public String toString() {
		return "ARG1 = ".concat(this.argument1).concat("\nARG2 = ").concat(this.argument2+"!");
	}

}
