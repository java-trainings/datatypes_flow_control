package net.curlybraces.classes_interfaces_inheritence.inheritence;

public class MultipleInterfaces implements Runnable, AutoCloseable {
	@Override
	public void run() {
		// Some implementation here
	}

	@Override
	public void close() throws Exception {
		// Some implementation here
	}
}
