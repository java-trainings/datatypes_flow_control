package net.curlybraces.lambda.optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;

public class OptionalTest {

	public String getDefaultValue() {
	    System.out.println("Getting Default Value");
	    return "Default Value";
	}
	
	@Test
	public void whenOrElseGetAndOrElseOverlap_thenCorrect() {
	    String text = null;
	 
	    String defaultText = Optional.ofNullable(text).orElseGet(this::getDefaultValue);
	    assertEquals("Default Value", defaultText);
	 
	    defaultText = Optional.ofNullable(text).orElse(getDefaultValue());
	    assertEquals("Default Value", defaultText);
	}
	
	@Test
	public void whenOrElseGetAndOrElseDiffer_thenCorrect() {
	    String text = "Text present";
	 
	    System.out.println("Using orElseGet:");
	    String defaultText 
	      = Optional.ofNullable(text).orElseGet(this::getDefaultValue);
	    assertEquals("Text present", defaultText);
	 
	    System.out.println("Using orElse:");
	    defaultText = Optional.ofNullable(text).orElse(getDefaultValue());
	    assertEquals("Text present", defaultText);
	}
	
	// Notice that when using orElseGet() to retrieve the wrapped value, the getMyDefault() method is not even invoked since the contained value is present.
	// However, when using orElse(), whether the wrapped value is present or not, the default object is created. 
	// So in this case, we have just created one redundant object that is never used.

	// In this simple example, there is no significant cost to creating a default object, as the JVM knows how to deal with such. 
	// However, when a method such as getMyDefault() has to make a web service call or even query a database, then the cost becomes very obvious.
}
