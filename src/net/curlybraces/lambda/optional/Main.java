package net.curlybraces.lambda.optional;

import java.util.Optional;

public class Main {

	public static void main(String[] args) {

		// We will create Optional objects using many ways

		// isPresent() : check if there is a value in the optional or not, returns true if the WRAPPED value is not null
		
		// Empty optional object
		Optional<String> empty = Optional.empty();
		System.out.println(empty.isPresent());

		
		// Optional object using "of" method
		// Can't pass null as a value => Generate an NPE
		String gender = "female";
		Optional<String> optWithOf = Optional.of(gender);
		System.out.println(optWithOf.isPresent() + " -- Optional value = " + optWithOf.get());
		
		
		// But, in case we expect some null values, we can use the ofNullable() method
		// it doesn�t throw an exception but rather returns an empty Optional object
		Optional<String> optWithOfNullable = Optional.ofNullable(null);
		System.out.println(optWithOfNullable.isPresent());
		
		
		// ifPresent() : enables us to run some code on the wrapped value if it�s found to be non-null
		// Let's analyze the code below
		String name = "John Doe";
		if(name != null) {
			System.out.println("Without Optional = " + name.length());
		}
		// 1- Check if variable is null 
		// 2- This approach is lengthy
		// 3- Prone to error
		// 4- Indeed, what guaranty us that, after printing that variable, we won�t use it again and then forget to perform the null check.
		// The code above is equivalent to :
		Optional.ofNullable(name).ifPresent(str -> System.out.println("With Optional = " + str.length()));
		
		
		// Default Value With orElse()
		// Retrieve the value wrapped inside an Optional instance if it�s present and its argument otherwise. 
		// It takes one parameter which acts as a default value
		name = null;
		String returnedValue = Optional.ofNullable(name).orElse("Default name");
		System.out.println("ReturnedValue = " + returnedValue);
		
		// Default Value With orElseGet()
		// Similar to orElse(). However, instead of taking a value to return if the Optional value is not present, 
		// it takes a supplier functional interface which is invoked and returns the value of the invocation
		String returnedValue2 = Optional.ofNullable(name).orElseGet(() -> "Default name 2");
		System.out.println("ReturnedValue2 = " + returnedValue2);
		
		// The difference between orElse() and orElseGet()
		// Refer to the OptionalTest class
		
		
		// Exceptions with orElseThrow()
		// The orElseThrow() method follows from orElse() and orElseGet() and adds a new approach for handling an absent value. 
		// Instead of returning a default value when the wrapped value is not present, it throws an exception
		name = null;
		String returnedValue3 = Optional.ofNullable(name).orElseThrow(() -> new IllegalArgumentException("Error: Value of name is not present"));
		System.out.println("ReturnedValue2 = " + returnedValue3);
		
	}

}
