package net.curlybraces.lambda.basics;

public interface DummyFunctionInterface {

	// create a method with no parameters & no return
	//void apply();
	
	// create a method with one parameter & no return
	//void apply(String text);
	
	// create a method with two parameters & no return
	//void apply(String text1, String textx2);
	
	// create a method with two parameters & a return
	//String apply(String text1, String textx2);
}
