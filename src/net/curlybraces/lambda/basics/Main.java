package net.curlybraces.lambda.basics;

import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		
		// implements compare method using an inner anonymous class
		/*Comparator<String> stringComparator = new Comparator<String>() {
			
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		};
		
		int result = stringComparator.compare("hello", "java 8");
		System.out.println("result = " + result);
		
		// with lambda expression
		Comparator<String> lambdaStringComparator = (str1, str2) -> str1.compareTo(str2);
		int lambdaResult = lambdaStringComparator.compare("hello", "java 8");
		System.out.println("Lambda result = " + lambdaResult);
		
		System.out.println("##########################################\n\n\n");*/
		
		
		//######## method with no parameters & no return
		/*DummyFunctionInterface dummyFunctionInterface = () -> { 
			System.out.println("Hello to Lambda expressions");
		};
		// or simply 
		// DummyFunctionInterface dummyFunctionInterface = () -> System.out.println("Hello to Lambda expressions");
		// without curly braces (if there is only one statement)
		dummyFunctionInterface.apply();*/
		
		//######## method with one parameter & no return
		/*DummyFunctionInterface dummyFunctionInterface = (text) -> System.out.println(text);
		// in the case of one parameter, we can omit the parenthesis and write simply
		//DummyFunctionInterface dummyFunctionInterface = text -> System.out.println(text);
		dummyFunctionInterface.apply("Hello again to lambdas");*/
		
		//######## method with two parameters & no return
		/*DummyFunctionInterface dummyFunctionInterface = (text1, text2) -> System.out.println(text1 + " - " + text2);
		// in the case of more than one parameter or no parameter, parenthesis are mandatory
		dummyFunctionInterface.apply("Hello for the third time to", "lambdas");*/
		
		//######## method with two parameters & a return
		/*DummyFunctionInterface dummyFunctionInterface = (text1, text2) -> text1 + " - " + text2;
		String result = dummyFunctionInterface.apply("Hello for the fourth time to", "lambdas");
		System.out.println("result = " + result);*/
		
		// lambda expressions are objects and can be assigned
		/*DummyFunctionInterface dummyFunctionInterface2 = dummyFunctionInterface;
		String result2 = dummyFunctionInterface2.apply("Hello for the fourth time to", "lambdas");
		System.out.println("result 2= " + result2);*/
	}
}
