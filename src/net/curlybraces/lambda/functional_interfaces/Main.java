package net.curlybraces.lambda.functional_interfaces;

import java.util.Arrays;
import java.util.List;

import net.curlybraces.interfaces.v2.InterfaceWithStaticDefaultMethods;

public class Main {

	public static void main(String[] args) {
		// implement the only abstract method in the InterfaceWithStaticDefaultMethods interface
		InterfaceWithStaticDefaultMethods interfaceWithStaticDefaultMethods = action -> System.out.println(action);
		interfaceWithStaticDefaultMethods.initAction("Manage Functional Interfaces");
		
		// You can always call default methods as before
		interfaceWithStaticDefaultMethods.performAnotherDefaulAction();
		
		// And access static methods using the interface
		InterfaceWithStaticDefaultMethods.secondAction();
		
		
		// Lambda implementation for generic interface
		GenericComputeInterface<String> reverse = (List<String> words) -> {
			for(int i=0; i < words.size(); i++) {
				String result = "";
				for(int j = words.get(i).length()-1; j >= 0; j--)
					result += words.get(i).charAt(j);
				words.set(i, result);
			}
			return words;
		};
		List<String> resultOfReverse = reverse.compute(Arrays.asList("car", "apple", "airplane"));
		System.out.println(resultOfReverse);
		
		GenericComputeInterface<Integer> square = (List<Integer> numbers) -> {
			for(int i=0; i < numbers.size(); i++) {
				numbers.set(i, numbers.get(i) * numbers.get(i)); 
			}
			return numbers;
		};
		List<Integer> resultOfSquare = square.compute(Arrays.asList(7, 11, 25));
		System.out.println(resultOfSquare);
	}
}
