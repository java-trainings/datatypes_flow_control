package net.curlybraces.lambda.functional_interfaces;

import java.util.List;

@FunctionalInterface
public interface GenericComputeInterface<T> {
	List<T> compute(List<T> t);
}
