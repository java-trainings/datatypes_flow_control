package net.curlybraces.lambda.stream;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) throws IOException {

		System.out.println("############ STREAM API ############\n\n");
		
		// Empty Stream
		// Its often the case that the empty() method is used upon creation to avoid returning null for streams with no element
		List<String> list = null;
		Stream<String> emptyStream = list == null || list.isEmpty() ? Stream.empty() : list.stream();
		System.out.println(emptyStream.count());
		
		
		// Stream of Collection
		// Stream can also be created of any type of Collection (Collection, List, Set)
		Collection<Integer> numbers = Arrays.asList(1, 2, 3);
		Stream<Integer> numbersStream = numbers.stream();
		System.out.println(numbersStream.count());
		
		
		// Stream of Array
		// Array can also be a source of a Stream
		Stream<String> streamOfArray = Stream.of("a", "b", "c", "d");
		System.out.println(streamOfArray.count());
		// The code above is also equivalent to : 
		String[] alphabets = new String[]{"a", "b", "c", "d"};
		Stream<String> streamOfArrayFull = Arrays.stream(alphabets);
		System.out.println(streamOfArrayFull.count());
		// They can also be created out of an existing array or of a part of an array
		Stream<String> streamOfArrayPart = Arrays.stream(alphabets, 1, 3);
		System.out.println(streamOfArrayPart.count());
		
		
		// Stream Generate
		// The generate() method accepts a Supplier<T> for element generation. 
		// As the resulting stream is infinite, developer should specify the desired size or the generate() 
		// method will work until it reaches the memory limit
		Supplier<Integer> supplier = () -> (int)(Math.random() * 10);
		Stream<Integer> nbrsStream = Stream.generate(supplier).limit(7);
		System.out.println(nbrsStream.count());
		//nbrsStream.forEach(nb -> System.out.println(nb));
		
		
		// Stream Iterate
		// Another way of creating an infinite stream
		// The first element of the resulting stream is a first parameter of the iterate() method. 
		// For creating every following element the specified function is applied to the previous element
		Stream<Integer> streamIterated = Stream.iterate(5, n -> n + 5).limit(10);
		System.out.println(streamIterated.count());
		//streamIterated.forEach(nb -> System.out.println(nb));
		
		
		// Stream of Primitives
		IntStream intStream = IntStream.range(1, 3);
		intStream.forEach(nb -> System.out.println(nb));
		
		LongStream longStream = LongStream.rangeClosed(1, 3);
		longStream.forEach(nb -> System.out.println(nb));
		// Using Random
		DoubleStream doubleStream = new Random().doubles().limit(2);
		doubleStream.forEach(nb -> System.out.println(nb));
		
		
		// Stream of File
		Path path = Paths.get("D:\\trainings\\java-8\\eclipse-workspace\\CompleteJavaMasterclass\\src\\net\\curlybraces\\lambda\\stream\\file.txt");
		Stream<String> streamWithCharset = Files.lines(path, Charset.forName("UTF-8"));
		streamWithCharset.forEach(line -> System.out.println(line));
		streamWithCharset.close();
	}

}
