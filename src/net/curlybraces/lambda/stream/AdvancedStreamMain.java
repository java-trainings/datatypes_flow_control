package net.curlybraces.lambda.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AdvancedStreamMain {

	public static void main(String[] args) {
		
		// Referencing a Stream
		// Java 8 streams can�t be reused.
		Stream<Integer> multiplesOfFiveStream = Stream.iterate(5, n -> n + 5).limit(6);
		// first operation on multiplesOfFiveStream
		//System.out.println(multiplesOfFiveStream.count());
		
		// Second operation
		// This second operation will generate a RuntimeException
		// java.lang.IllegalStateException: stream has already been operated upon or closed
		//Optional<Integer> firstElement = multiplesOfFiveStream.findFirst();
		
		// This kind of behavior is logical because streams were designed to provide an ability 
		// to apply a finite sequence of operations to the source of elements in a functional style, but not to store elements
		// Make the old code works (Comment the above code)
		List<Integer> numbersList = multiplesOfFiveStream.collect(Collectors.toList());
		System.out.println(numbersList.size());
		Optional<Integer> firstElement = numbersList.stream().findFirst();
		System.out.println(firstElement.orElse(-1));
		
		
		// Terminal operations versus Non terminal operations
		List<String> strs = Arrays.asList("item1", "item2", "item3");
		strs.stream().map(el -> el.toUpperCase()).forEach(el -> System.out.println(el));
		// More concise
		//strs.stream().map(String::toUpperCase).forEach(System.out::println);
		// Let's analyze the code above
		// Create the stream
		Stream<String> strsInitialStream = strs.stream();
		// Applying a no terminal operation (map)
		Stream<String> strsUpperCaseStream = strsInitialStream.map(el -> el.toUpperCase());
		// Applying a terminal operation (forEach)
		strsUpperCaseStream.forEach(el -> System.out.println(el));

	}

}
