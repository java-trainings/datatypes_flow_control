package net.curlybraces.lambda.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class LazyInvocationStreamMain {

	private static int counter = 0;
	
	private static void wasCalled() {
		counter++;
		System.out.println("counter = " + counter);
	}
	
	public static void main(String[] args) {

		// Lazy Invocation
		// Intermediate operations are lazy. This means that they will be invoked only if it is necessary for the terminal operation execution
		List<String> strs = new ArrayList<String>(Arrays.asList("item1", "item2", "item3"));
		Stream<String> stream = strs.stream().filter(element -> {
		    wasCalled();
		    return element.contains("2");
		});
		
		// Executing the code above does not print anything
		// lambda function passed as parameter to intermediate operation "filter" never executed
		
		//Let's modify our code
		strs.stream().filter(element -> {
		    wasCalled();
		    return element.contains("2");
		}).map(element -> {
		    System.out.println("map() was called");
		    return element.toUpperCase();
		}).findFirst().ifPresent(System.out::println);
		
		//Let's analyze the output
		// wasCalled() was called twice (until the predicate is satisfied), that means the filter() method was called twice also 
		// and the map() method just once. It is so because the pipeline executes vertically. 
		// In our example the first element of the stream didn�t satisfy filter�s predicate, then the filter() method was invoked for the second element, 
		// Without calling the filter() for third element we went down through pipeline to the map() method.

		// The findFirst() operation satisfies by just one element. 
		// So, in this particular example the lazy invocation allowed to avoid two method calls � one for the filter() and one for the map().
		
		//Let's modify our code
		strs.add("item2.1");
		strs.stream().filter(element -> {
		    wasCalled();
		    return element.contains("2");
		}).map(element -> {
		    System.out.println("map() was called");
		    return element.toUpperCase();
		}).forEach(System.out::println);
	}

}
