package net.curlybraces.lambda.variables_capture;

public class Main {

	// static variable
	static String textfr = "Bonjour";
	// instance variable
	String texten = "Hello";

	public static void main(String[] args) {
		// Using a static variable
		/*DummyVarCaptureInterface dummyVarCaptureInterface = text -> System.out.println(textfr.concat(" ").concat(text));
		dummyVarCaptureInterface.printText("Mohamed");
		textfr = "Salut";
		dummyVarCaptureInterface.printText("Mohamed");*/
		
		// Using an instance variable
		/*Main main = new Main();
		DummyVarCaptureInterface dummyVarCaptureInterface = text -> System.out.println(main.texten.concat(" ").concat(text));
		dummyVarCaptureInterface.printText("Mohamed");
		main.texten = "Hi";
		dummyVarCaptureInterface.printText("Mohamed");*/
		
		// Using a local variable
		/*String textar = "Hala";
		DummyVarCaptureInterface dummyVarCaptureInterface = text -> System.out.println(textar.concat(" ").concat(text));
		dummyVarCaptureInterface.printText("Mohamed");
		textar = "Salam";*/
	}

}
