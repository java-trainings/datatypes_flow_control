package net.curlybraces.lambda.variables_capture;

public interface DummyVarCaptureInterface {

	void printText(String text);
}
