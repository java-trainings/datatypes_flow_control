package net.curlybraces.interfaces.v2;

public class ClassImplementsInterfaceDefaultMethods implements InterfaceWithStaticDefaultMethods {

	@Override
	public void initAction(String action) {
		System.out.println("contract method");
	}
	
	@Override
	public String performDefaulAction() {
		return InterfaceWithStaticDefaultMethods.super.performDefaulAction().concat(" overridden !!");
	}
}
