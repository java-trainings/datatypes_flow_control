package net.curlybraces.interfaces.v2;

public class Main {

	public static void main(String[] args) {
		System.out.println("Java 8 interfaces evols\n\n");
		
		// static methods from interface
		System.out.println(InterfaceWithStaticDefaultMethods.action());
		System.out.println(InterfaceWithStaticDefaultMethods.secondAction());
		
		// default methods
		ClassImplementsInterfaceDefaultMethods classImplementsInterfaceDefaultMethods = new  ClassImplementsInterfaceDefaultMethods();
		System.out.println(classImplementsInterfaceDefaultMethods.performDefaulAction());
		System.out.println(classImplementsInterfaceDefaultMethods.performAnotherDefaulAction());
	}
}
