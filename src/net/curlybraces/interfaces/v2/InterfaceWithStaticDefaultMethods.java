package net.curlybraces.interfaces.v2;

@FunctionalInterface
public interface InterfaceWithStaticDefaultMethods {

	// In case more than one abstract methods are present, 
	// the compiler flags an ‘Unexpected @FunctionalInterface annotation’ message. 
	// However, it is not mandatory to use this annotation.
	static String action() {
		return "action from interface";
	}

	static String secondAction() {
		return "second action from interface";
	}

	default String performDefaulAction() {
		return "default action";
	}
	
	default String performAnotherDefaulAction() {
		return "another default action";
	}
	
	void initAction(String action);
	
}
