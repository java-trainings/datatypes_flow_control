package net.curlybraces.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		// List
		List<String> listOfColors1 = new ArrayList<String>();
		listOfColors1.add("red");
		listOfColors1.add("white");
		listOfColors1.add("yellow");
		listOfColors1.add("brown");
		// Another way to initialize a list
		List<String> listOfColors2 = Arrays.asList("white", "red", "yellow", "brown");

		System.out.println(listOfColors1);
		System.out.println(listOfColors2);
		// iterate over a list using for loop
		for (String s: listOfColors1) {
			System.out.println(s);
		}
		// loop using Iterator interface
		Iterator<String> iter = listOfColors2.iterator();
		while(iter.hasNext()) {
			System.out.println(iter.next());
		}
		
		Map<String, String> langs = new HashMap<String, String>();
		langs.put("Kotlin", "Google");
		langs.put("ReactJs", "Facebook");
		langs.put("Java", "Oracle");
		langs.put("Typescript", "Microsoft");
		
		// iterate over a map using for loop
		for (Map.Entry<String, String> entry : langs.entrySet()) {
		    System.out.println(entry.getKey() + "/" + entry.getValue());
		}
		
		// loop using Iterator interface
		Iterator<Map.Entry<String, String>> it = langs.entrySet().iterator();
		while (it.hasNext()) {
		    Map.Entry<String, String> pair = it.next();
		    System.out.println(pair.getKey() + "//" + pair.getValue());
		}
		
	}

}
