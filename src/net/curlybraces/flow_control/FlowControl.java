package net.curlybraces.flow_control;

import java.time.DayOfWeek;

public class FlowControl {

	public static void main(String[] args) {
		System.out.println("Flow Control Chapter\n\n");

		int val = (int) Math.round(Math.random() * 6);
		String day = printTheDayOfTheWeek(val);
		System.out.println("Day of the week for " + val + " is :: " + day);
		
		switch (val) {
		case 1: 
			System.out.println("val is equal to 1");
			break;
		case 2:
			System.out.println("val is equal to 2");
			break;
		case 3: case 4: case 5:
			System.out.println("val is greater or equal to 3 (" + val + ")" );
			break;
		default:
			System.out.println("val is equal to " + val);
			break;
		}
		
		System.out.println("############## Challenge ##############");
		int count = 0, sum = 0;
		for (int i = 1; i <= 1000; i++) {
			if(i % 3 == 0 && i % 5 == 0) {
				count++;
				sum +=i;
				System.out.println("i :: " + i);
			}
			
			if(count == 5) {
				break;
			}
		}
		
		System.out.println("Sum :: " + sum);
	}
	
	private static String printTheDayOfTheWeek(int val) {
		if(val < 0 || val > 6) {
			return "Error";
		}
		
		return DayOfWeek.of(val).name();
	}
}
