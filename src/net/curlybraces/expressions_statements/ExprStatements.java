package net.curlybraces.expressions_statements;

public class ExprStatements {

	public static void main(String[] args) {
		
		System.out.println("Expressions, Statements Chapter\n\n");
		int a = 9;
		boolean bool = true;
		if(bool) {
			System.out.println((a +1) + " This is an expression ");
		}
		
	}

}
